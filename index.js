var app = require('express')();
var session = require('express-session');
var bodyParser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql=require('mysql');
var con =mysql.createConnection({
host:"127.0.0.1",
port:"3306",
user:"root",
password:"",
database: "chat"
});

con.connect(function(err) {
    if (err) console.log(err);
});

var users=[];
var activeuser=[];

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(session({secret: 'ssshhhhh'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var sess;
var mobile;
app.get('/', function(req, res){
  sess = req.session;
  if(sess.mobile)
  res.render(__dirname + '/index.html',{
    mobile:sess.mobile
  });
  else {
  res.render(__dirname+'/login.html')
  }
});

app.post('/login',function(req,res){
sess = req.session;
  sess.mobile=req.body.mobile;
  mobile=req.body.mobile;
  var pass=req.body.pass;
  var sql  = "SELECT * FROM `users` WHERE `Mobile`='"+mobile+"' AND `Password`='"+pass+"'";
    con.query(sql, function(err,result){
      if(err) throw err;
      if(result.length>0){
        console.log("login successful");
        //sess = req.session;
        //sess.mobile=mobile;
        res.end('done');
      }
      else {
        res.end('fail')
      }
    });

});

app.post('/register',function(req,res){

con.query("SELECT COUNT(*) as count from users WHERE `Mobile`='"+req.body.rmobile+"'",function(err,result){
  if(result[0].count==0){
    con.query("INSERT INTO `users`(`Mobile`, `Password`) VALUES ('"+req.body.rmobile+"','"+req.body.rpass+"')", function(err,result){
      if(err) throw err;
      if(result.affectedRows==1)
      {
        console.log('user added');
        res.end('done');
      }
    });
  }
  else {
    res.end('fail');
  }

})
});

app.get('/logout',function(req,res){
    req.session.destroy(function(err){
        if(err){
            console.log(err);
        }
        else
        {
            res.redirect('/');
        }
    });

});

io.on('connection', function(socket){
    if(mobile!=undefined){
    users[mobile]=socket.id;
    activeuser.push(mobile);
    con.query("SELECT * FROM `chats` WHERE `ToUser`='GROUP'", function(err,groupchat){
      if(err) throw err;
      con.query("SELECT * FROM `chats` WHERE `ToUser`='"+mobile+"'",function(err,personalchat){
        io.emit('new user',activeuser,groupchat,personalchat);
      });
    });

    }

  /*socket.on('new user',function(newuser){
    sess.mobile=newuser;
	 users[newuser]= socket.id;
	 io.emit('new user',newuser);
 });*/

	  socket.on('private message',function(from,to,msg){
			console.log(msg);
			console.log(users);
      con.query("INSERT INTO `chats`(`FromUserName`, `ToUser`, `Message`) VALUES ('"+from+"','"+to+"','"+msg+"')", function(err,result){
        if(err) throw err;
        if(result.affectedRows==1)
        {
          console.log('msg stored');
        }
      });
		  io.to(users[to]).emit('private message',from,to,msg);
      io.to(users[from]).emit('private message',from,to,msg);
    });

	  socket.on('group message',function(from,msg){
		  console.log(msg);
			console.log(users);
      con.query("INSERT INTO `chats`(`FromUserName`, `ToUser`, `Message`) VALUES ('"+from+"','GROUP','"+msg+"')", function(err,result){
        if(err) throw err;
        if(result.affectedRows==1)
        {
          console.log('msg stored');
        }
      });
	  io.emit('group message',from,msg);
  });
});
http.listen(3000, function(){
  console.log('listening on *:3000');
});
